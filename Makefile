GOARCH = amd64

UNAME = $(shell uname -s)

ifndef OS
	ifeq ($(UNAME), Linux)
		OS = linux
	else ifeq ($(UNAME), Darwin)
		OS = darwin
	endif
endif

.DEFAULT_GOAL := all

all: fmt build start

build:
	GOOS=$(OS) GOARCH="$(GOARCH)" go build -o vault/plugins/vault-plugin-kerberos-secrets cmd/vault-plugin-kerberos-secrets/main.go

start:
	vault server -dev -log-level=debug -dev-root-token-id=root -dev-plugin-dir=./vault/plugins

integration-test: enable test-config test-role test-cred

enable:
	vault secrets enable -path=krb vault-plugin-kerberos-secrets

test-config:
	vault write krb/config realm=TARDISPROJECT.UK kdc=localhost:88 kadmin_server=localhost:749 username=test/admin password=1234

test-role:
	vault write krb/static-role/test principal=test

test-rotate:
	vault write -f krb/rotate-static-role/test

test-rotate-root:
	vault write -f krb/rotate-root

test-cred:
	vault read krb/static-cred/test

clean:
	rm -f ./vault/plugins/vault-plugin-kerberos-secrets

fmt:
	go fmt $$(go list ./...)

.PHONY: build clean fmt start enable test_config
