package secretsengine

import (
	"context"
	"errors"
	"fmt"
	"time"

	"git.tardisproject.uk/tcmal/vault-plugin-kerberos-secrets/config"
	"git.tardisproject.uk/tcmal/vault-plugin-kerberos-secrets/password"
	"github.com/hashicorp/vault/sdk/framework"
	"github.com/hashicorp/vault/sdk/logical"
)

const (
	rotateRootPath = "rotate-root"
	rotateRolePath = "rotate-static-role/"
)

func pathRotateCredentials(b *krbBackend) []*framework.Path {
	return []*framework.Path{
		{
			Pattern: rotateRootPath,
			Fields:  map[string]*framework.FieldSchema{},
			Operations: map[logical.Operation]framework.OperationHandler{
				logical.UpdateOperation: &framework.PathOperation{
					Callback:                    b.pathRotateRootCredentialsUpdate,
					ForwardPerformanceStandby:   true,
					ForwardPerformanceSecondary: true,
				},
			},
			HelpSynopsis: "Request to rotate the root credentials Vault uses for the kerberos administrator account.",
			HelpDescription: "This path attempts to rotate the root credentials of the administrator account " +
				"used by Vault to manage credentials.",
		},
		{
			Pattern: rotateRolePath + framework.GenericNameRegex("name"),
			Fields: map[string]*framework.FieldSchema{
				"name": {
					Type:        framework.TypeString,
					Description: "Name of the static role",
				},
			},
			Operations: map[logical.Operation]framework.OperationHandler{
				logical.UpdateOperation: &framework.PathOperation{
					Callback:                    b.pathRotateRoleCredentialsUpdate,
					ForwardPerformanceStandby:   true,
					ForwardPerformanceSecondary: true,
				},
			},
			HelpSynopsis:    "Request to rotate the credentials for a static user account.",
			HelpDescription: "This path attempts to rotate the credentials for the given static role.",
		},
	}
}

func (b *krbBackend) pathRotateRootCredentialsUpdate(ctx context.Context, req *logical.Request, data *framework.FieldData) (*logical.Response, error) {
	if _, hasTimeout := ctx.Deadline(); !hasTimeout {
		var cancel func()
		ctx, cancel = context.WithTimeout(ctx, defaultCtxTimeout)
		defer cancel()
	}

	config, err := getConfig(ctx, req.Storage)
	if err != nil {
		return nil, err
	}
	if config == nil {
		return nil, errors.New("the config is currently unset")
	}

	client, err := b.getClient(ctx, req.Storage)
	if err != nil {
		return nil, err
	}

	newPassword := password.Generate()

	// Update the password remotely.
	if err := (*client).SetPassword(ctx, config.Username, newPassword); err != nil {
		return nil, err
	}

	// Update the password locally.
	config.Password = newPassword
	if pwdStoringErr := storePassword(ctx, req.Storage, config); pwdStoringErr != nil {
		return nil, fmt.Errorf("unable to update password due to storage err: %s", pwdStoringErr)
		// TODO: deal with this more gracefully
	}

	b.reset()

	// Respond with a 204.
	return nil, nil
}

func (b *krbBackend) pathRotateRoleCredentialsUpdate(ctx context.Context, req *logical.Request, data *framework.FieldData) (*logical.Response, error) {
	name := data.Get("name").(string)
	if name == "" {
		return logical.ErrorResponse("empty role name attribute given"), nil
	}

	role, err := b.getRole(ctx, req.Storage, name)
	if err != nil {
		return nil, fmt.Errorf("error fetching role from storage: %s", err)
	}
	if role == nil {
		return nil, fmt.Errorf("role does not exist: %s", name)
	}

	b.backgroundRotation(name, req.Storage)

	return nil, nil
}

func (b *krbBackend) backgroundRotation(name string, storage logical.Storage) {
	// make sure we're not in the rotation list already, and add us if not.
	// this ensures we dont have two credential rotations running for the same principal
	// at the same time.
	b.rotationListLock.Lock()

	_, ok := b.rotationList[name]
	if ok {
		b.rotationListLock.Unlock()
		b.Logger().With("role", name).Debug("rotation already in progress, not starting another one")
		return
	}

	b.rotationList[name] = true

	// done adding to list
	b.rotationListLock.Unlock()

	ictx, _ := context.WithTimeout(context.Background(), time.Minute*60)
	go b.doRotation(ictx, name, storage)
}

func (b *krbBackend) doRotation(ctx context.Context, name string, storage logical.Storage) {
	log := b.Logger().With("role", name)
	log.Debug("starting to rotate role")

	defer func() {
		// remove from rotation list
		b.rotationListLock.Lock()
		if _, ok := b.rotationList[name]; ok {
			delete(b.rotationList, name)
		}
		b.rotationListLock.Unlock()
	}()

	for {
		err, retry := b.attemptRotation(ctx, name, storage) // TODO
		if err == nil {
			log.Debug("credentials rotated")
			return
		}

		log.Error("error rotating credentials", "error", err)
		if !retry {
			log.Error("unrecoverable error rotating credentials")
		}

		timer := time.NewTimer(5 * time.Second)
		select {
		case <-timer.C:
			continue
		case <-ctx.Done():
			log.Error("timeout rotating credentials")
			return
		}
	}
}

func (b *krbBackend) attemptRotation(ctx context.Context, name string, storage logical.Storage) (error, bool) {
	role, err := b.getRole(ctx, storage, name)
	if err != nil {
		return fmt.Errorf("error fetching role from storage: %s", err), true
	}
	if role == nil {
		return fmt.Errorf("role does not exist: %s", name), false
	}

	c, err := b.getClient(ctx, storage)
	if err != nil {
		return fmt.Errorf("error getting client: %s", err), true
	}

	newPassword := password.Generate()
	err = (*c).SetPassword(ctx, role.Principal, newPassword)
	if err != nil {
		return fmt.Errorf("error setting password: %s", err), true
	}

	role.Password = newPassword
	role.LastVaultRotation = time.Now()
	err = setRole(ctx, storage, name, role)
	if err != nil {
		return fmt.Errorf("rotated password but could not save back to storage"), true
		// TODO: deal with this more gracefully
	}

	return nil, false
}

func storePassword(ctx context.Context, s logical.Storage, cfg *config.Config) error {
	entry, err := logical.StorageEntryJSON(configStoragePath, cfg)
	if err != nil {
		return err
	}
	return s.Put(ctx, entry)
}
