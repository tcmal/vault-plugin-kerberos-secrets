package secretsengine

import (
	"context"
	"fmt"
	"strings"
	"sync"

	"git.tardisproject.uk/tcmal/vault-plugin-kerberos-secrets/client"
	"github.com/hashicorp/vault/sdk/framework"
	"github.com/hashicorp/vault/sdk/logical"
)

// krbBackend wraps the krbBackend framework and adds a map for storing key value pairs
type krbBackend struct {
	*framework.Backend
	client KerberosClient

	rotationListLock *sync.Mutex
	rotationList     map[string]bool
}

type KerberosClient interface {
	SetPassword(ctx context.Context, username string, password string) error
}

var _ logical.Factory = Factory

// Factory configures and returns Mock backends
func Factory(ctx context.Context, conf *logical.BackendConfig) (logical.Backend, error) {
	b := newBackend()

	if conf == nil {
		return nil, fmt.Errorf("configuration passed into backend is nil")
	}

	if err := b.Setup(ctx, conf); err != nil {
		return nil, err
	}

	return b, nil
}

func newBackend() *krbBackend {
	b := &krbBackend{
		rotationListLock: &sync.Mutex{},
		rotationList:     map[string]bool{},
	}
	b.Backend = &framework.Backend{
		Help:        strings.TrimSpace(mockHelp),
		BackendType: logical.TypeLogical,
		Paths: framework.PathAppend(
			pathConfig(b),
			pathStaticRole(b),
			pathStaticCreds(b),
			pathRotateCredentials(b),
		),
	}

	return b
}

// reset clears any client configuration for a new
// backend to be configured
func (b *krbBackend) reset() {
	b.client = nil
}

// invalidate clears an existing client configuration in
// the backend
func (b *krbBackend) invalidate(ctx context.Context, key string) {
	if key == "config" {
		b.reset()
	}
}

func (b *krbBackend) getClient(ctx context.Context, s logical.Storage) (*KerberosClient, error) {
	if b.client == nil {
		c, err := getConfig(ctx, s)
		if err != nil {
			return nil, err
		}

		client, err := client.ClientFromConfig(c)
		if err != nil {
			return nil, err
		}

		b.client = client
	}

	return &b.client, nil
}

const mockHelp = `
The Kerberos backend is a backend that sets credentials in kerberos.
`
