package password

import (
	"math/rand"
)

// Generate randomly generates a secure password
func Generate() string {
	pass := ""
	for i := 0; i < 24; i++ {
		pass = pass + string(RandChar())
	}
	return pass
}

func RandChar() int {
	typ := rand.Intn(3)
	if typ == 0 {
		// 48-57, numbers
		return rand.Intn(10) + 48
	} else if typ == 1 {
		// 65-90, uppercase
		return rand.Intn(26) + 65
	} else {
		// 97-122, lowercase
		return rand.Intn(26) + 97
	}
}
