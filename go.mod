module git.tardisproject.uk/tcmal/vault-plugin-kerberos-secrets

go 1.12

require (
	github.com/hashicorp/go-hclog v0.14.1
	github.com/hashicorp/vault/api v1.0.5-0.20210325191337-ac5500471f36
	github.com/hashicorp/vault/sdk v0.1.14-0.20210325185647-d3758c9bd369
	github.com/hashicorp/yamux v0.0.0-20181012175058-2f1d1f20f75d // indirect
	github.com/jcmturner/gokrb5 v8.4.4+incompatible
	github.com/jcmturner/gokrb5/v8 v8.4.4
)
