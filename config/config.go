package config

type Config struct {
	Realm        string   `json:"realm"`
	KDC          []string `json:"kdc"`
	KAdminServer string   `json:"kpasswd_server"`

	Username string `json:"username"`
	Password string `json:"password"`
}
