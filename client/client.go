package client

import (
	"context"
	"fmt"
	"sync"

	"git.tardisproject.uk/tcmal/vault-plugin-kerberos-secrets/config"
)

type client struct {
	*sync.Mutex

	config *config.Config
}

func ClientFromConfig(config *config.Config) (client, error) {
	return client{
		&sync.Mutex{},
		config,
	}, nil
}

func (c client) SetPassword(ctx context.Context, username string, password string) error {
	c.Lock()
	defer c.Unlock()

	// check if the principal exists
	exists, err := c.princExists(ctx, username)
	if err != nil {
		return fmt.Errorf("error checking principal exists: %s", err)
	}

	if !exists {
		// if not, create it
		err = c.doCreatePrinc(ctx, username, password)
	} else {
		// otherwise, just set the password
		err = c.doChangePassword(ctx, username, password)
	}

	return err
}
