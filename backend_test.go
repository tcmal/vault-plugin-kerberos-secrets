package secretsengine

import (
	"context"
	"fmt"

	log "github.com/hashicorp/go-hclog"
	"github.com/hashicorp/vault/sdk/helper/logging"
	"github.com/hashicorp/vault/sdk/logical"
)

func getBackend() (*krbBackend, logical.Storage) {
	config := &logical.BackendConfig{
		Logger:      logging.NewVaultLogger(log.Debug),
		StorageView: &logical.InmemStorage{},
	}

	b := newBackend()
	b.Setup(context.Background(), config)

	return b, config.StorageView
}

var _ KerberosClient = fakeLdapClient{}

type fakeLdapClient struct {
	passwords map[string]string
}

func (c fakeLdapClient) SetPassword(username string, password string) error {
	c.passwords[username] = password
	return nil
}

func (c fakeLdapClient) SetPasswordWithOld(username string, oldPassword, newPassword string) error {
	if realOldPassword, ok := c.passwords[username]; ok && oldPassword != realOldPassword {
		return fmt.Errorf("invalid old password")
	}
	c.passwords[username] = newPassword
	return nil
}
